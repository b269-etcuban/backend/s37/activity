const Course = require("../models/Course");


// create a new course
// module.exports.addCourse = (reqBody) => {
// 	let newCourse = new Course ({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	});
// 	return newCourse.save().then( (course, error) => {
// 		// Course creation failed
// 		if (error) {
// 			return false;
// 		// course creation successful
// 		} else {
// 			return true;
// 		};
// 	});
// };


module.exports.addCourse = (data) => {
	if(data.isAdmin) {
		let newCourse = new Course ({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});
		return newCourse.save().then( (course, error) => {
			// Course creation successful
			if (error) {
				return false;
			// course creation failed
			} else {
				return true;
			};
		});
	};
	// user is not an admin
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	})
	
};

// retrieve ALL courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// retrieve active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

// retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdAndUpdate(document ID, updatesTobeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true
		}
	})
}


// updating status
module.exports.updateActive = (reqParams, reqBody) => {
	let updatedStatus = {
		isActive: reqBody.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedStatus).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true
		}
	})
}


