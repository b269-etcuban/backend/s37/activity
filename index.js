// setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const courseRoute = require("./routes/courseRoute");

// server setup
const app = express();

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// allows all the user routes created in "userRoute.js" file to use "/users" as route (resources)
app.use("/users", userRoute);
app.use("/courses", courseRoute);

// Database connection
mongoose.connect("mongodb+srv://ecriszianne:admin123@zuitt-bootcamp.tjdoe16.mongodb.net/s37-s41?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
 
mongoose.connection.once("open", () => console.log(`Now connected to the cloud database`));

// Server listening
// Will useed the defined port number for the application whenever environment variable is available to used port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`));