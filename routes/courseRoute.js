const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseController");

const auth = require('../auth');

// Route for creating a course
// router.post("/create", (req, res) => {
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
// });

router.post("/create", auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});

router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

router.get("/active", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

//route for retrieving specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})


// router for updating a course
router.put("/:courseId", (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})



router.patch("/:courseId", auth.verify, (req, res) => {
	courseController.updateActive(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;

